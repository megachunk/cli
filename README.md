# README #

### What is this repository for? ###
  This tool is a simple command line tool, aiming to quickly set up the project structure for Advanced Analytics tools. Current version 0.1
  
  When the tool is run, it performs the following setup steps:

  1. It checks the command input to verify if user has specified new project output path and the Bitbucket repo.
  2. It defaults the project output path to user's home folder, if output path was not specified by the user.
  3. It asks for the repo link, if the Bitbucket repo link was not provided as command line input.
  4. It clones the repo into RStudio Server local environment given the repo link is available.
  5. It setups the folder structure for the project.
  6. It copies in the boilerplate code such as git ignore file, YAML configuration file, Sharepoint API files and deployment script.
  7. It saves all the changes above into the first commit.
  8. It creates all the branches, in line with Advanced Analytics branching strategy. All branches will carry the first/initial commits.
  9. It moves the project folder to the location specified by the user.
  10.It emits final message "Project Initiation is complete", signifying that the process is complete, otherwise the process disrupts early.

### How do I get set up? ###

*How to use?

  1. Open up a terminal in RStudio Server(either in RSS or other type of terminal)
  2. Install this tool in your RStudio Server Home folder by using command terminal
  
    git clone git@bitbucket.org:megachunk/cli.git
  3. Get into the CLI folder
  
    cd ~/cli
  4. Run the CLI by providing your repo link and destination project output folder path, if you are not sure, see the test example below
  
    Rscript cli.R remote=<repo link from BitBucket> dest=<output folder path>
  
*Configuration

 No additional Configuration is required. As the program will check and install the required dependencies.
  
*Dependencies
 
 Only library crayon is required for terminal text ASCII enhancement. It will be installed when the tool is run.

*Test example

  You could use this test repo:
  
  ```git@bitbucket.org:megachunk/test2.git```
  
  In the CLI folder, please run:
  
  ```Rscript cli.R remote=git@bitbucket.org:megachunk/test2.git dest=~/```
  
  This will create a test2 project folder in your home folder.


### Who do I talk to? ###

* Repo owner or admin
  This tool is solely maintained by Patrick Guorong Chen at the moment.
